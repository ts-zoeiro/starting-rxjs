import { Component } from "./Component";
import {Observable} from "Rxjs";

export class DebounceTime {
    component: Component;
    constructor(){

        this.component = new Component();

        var input = document.createElement("input");
        input.classList.add("hello");
        this.component.appendChild(input);

        var observable = Observable.fromEvent(input, "input");

        observable
            .map( (event: any) => event.target.value)
            .debounceTime(400)
            .distinctUntilChanged()
            .subscribe({
                next: function(value: any) {
                    console.log(value);
                }
            })
        
    }
}