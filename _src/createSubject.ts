// import {Subject} from "Rxjs/Subject";
import {Subject} from 'Rxjs';

export class CreateSubject {
    subject = new Subject();
    //Subject inherits from Observable
    //it`s capable to call next() nanually
    constructor() {
        this.subject = new Subject();
        console.log("Constructor");
        this.subject.subscribe({
            next: function(value: any) {
                console.log(value);
            },

            error: function(error: any) {
                console.log(error);
            },

            complete: function() {
                console.log("Complete");
            }

        });

        this.subject.subscribe({
            next: function (value: any) {
                console.log(value);
            },
            error: function(error: any){
                console.log(error);
            }
        });

        this.subject.next("A new data piece");
        this.subject.error("error");

    }
}