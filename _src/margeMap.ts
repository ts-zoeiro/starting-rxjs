import { Component } from "./Component";
import {Observable} from "Rxjs";

export class MergeMap {
    component: Component;
    constructor(){
        this.component = new Component();

        var input1 = document.createElement("input");
        input1.classList.add("hello");
        this.component.appendChild(input1);

        var input2 = document.createElement("input");
        input2.classList.add("hello");
        this.component.appendChild(input2);

        var p = document.createElement("p");
        var span = document.createElement("span");
        p.innerHTML = "Combined value:"
        p.appendChild(span);
        this.component.appendChild(p);

        var observable1 = Observable.fromEvent(input1, "input");
        var observable2 = Observable.fromEvent(input2, "input");

        observable1
            .mergeMap( (event1: any) => {
                return observable2.map((event2: any) => {
                    return event1.target.value + " " + event2.target.value
                })
            })
            .subscribe((combinedValue: any) => {
                span.textContent = combinedValue;
            })

    }
}