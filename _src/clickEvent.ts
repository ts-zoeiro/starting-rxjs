import { Component } from "./Component";
import {Observable} from "Rxjs";

export class ClickEvent {
    component: Component;
    constructor() {

        this.component = new Component();

        var vanillaBtn = document.createElement("button");
        vanillaBtn.innerHTML = "vanilla - click me";
        vanillaBtn.classList.add('hello');
        this.component.appendChild(vanillaBtn);
    
        //vanilla code
        var count = 0;
        var rate = 1000;
        var lastClick = Date.now() - rate;
    
        vanillaBtn.addEventListener("click", (event) => {
            if (Date.now() - lastClick >= rate) {
                console.log(`Clicked ${++count} times`);
                lastClick = Date.now();
                lastClick = Date.now();
            }
        });
    
    
        //rxjs 
        let rxBtn  = document.createElement("button");
        rxBtn.innerHTML = "rxjs - click me";
        rxBtn.classList.add('hello');
        this.component.appendChild(rxBtn);
        let rxClickCount = 0; 
        
        Observable.fromEvent(rxBtn, "click")
            .throttleTime(1000)
            .map((data: any) => {
                return data.clientY
            })
            .subscribe((cordinate: any) => {
                console.log(`cordinate: ${cordinate}`);
            });
    }
}