import {Observable} from "Rxjs";

export class SomeOperators {

    constructor(){
                
        let observable = Observable.interval(1000);
        let observer = {
            next: function (value: any) {
                console.log(value);
            }
        }

        let subscription = observable.map(function (value: any){
            return "Number: " + value;
        })
        .throttleTime(2000)
        .subscribe(observer);

        setTimeout(function(){subscription.unsubscribe();}, 15000)

    }
}
