import {Observable} from "Rxjs";
import { Component } from "./Component";

export class CreateObservable {
    component: Component;

    constructor() {

        this.component = new Component();

        //manually creating Obeserver 
        //this object will implement 3 possible functons
        let observer = {
            next: function(value: any) {
                console.log(value)
            },
            error: function(error: any){
                console.log(error);
            },
            complete: function(){
                console.log("Completed!");
            }
        }


        //creating custom observable
        Observable.create(function(obs: any){
            obs.next("Some value");
            // obs.error({success: false, message: "server error"})
            obs.complete();
            obs.next("Some value 2");
        })
        .subscribe(observer)


        //using assyncronous code
        Observable.create(function (obs: any){
            obs.next("first value")
            setTimeout(function (){
                obs.complete();
            }, 2000);

            obs.next("second value");
        })
        .subscribe(observer);

        // //using the custom observer with a button click event
        let button = document.createElement("button");
        button.innerHTML = "creating observer";

        this.component.appendChild(button);
        let subscription1 = Observable.fromEvent(button, "click")
            .subscribe(observer)


        //recreating .fromEvent method
        let subscription2 = Observable.create(function(obs : any) {
            button.onclick = function (event) {
                obs.next(event);
                obs.complete();
            }
        })
        .subscribe(observer);


        setTimeout(function () {
            subscription1.unsubscribe();
            subscription2.unsubscribe();
        }, 5000)

    }
}
