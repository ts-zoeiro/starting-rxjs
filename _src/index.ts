// import _  from "lodash";
import './style.css';
import {Observable} from "Rxjs";
import { CreateSubject } from './createSubject';
import { SomeOperators } from './someOperators';
import { CreateObservable } from './createObservable';
import { ClickEvent } from './clickEvent';
import { Component } from './Component';
import { FilterOperator } from './fiterOperator';
import { DebounceTime } from './debounceTime';
import { ScanVsReduce } from './scanVsReduce';
import { Pluck } from './pluck';
import { MergeMap } from './margeMap';
import { SwitchMap } from './switchMap';
import { BehaviourSubject } from './behaviourSubject';
import { CreateBehaviorSubject } from './behaviorSubject';

export class Index {
    clickEvent: ClickEvent;
    createObservable: CreateObservable;
    someOperators: SomeOperators;
    createSubject: CreateSubject;
    filterOperator: FilterOperator;
    debounceTime: DebounceTime;
    scanVsReduce: ScanVsReduce;
    pluck: Pluck;
    mergeMap: MergeMap;
    switchMap: SwitchMap;
    behaviourSubject: CreateBehaviorSubject;

    constructor() {
        //Handlig click event with vannila VS subscribing observable from click Event
        // this.clickEvent = new ClickEvent();
        //Manually creating Observables
        // this.createObservable = new CreateObservable();
        //showing some operators
        // this.someOperators = new SomeOperators();
        //creating subject manually
        // this.createSubject = new CreateSubject();

        //the filter operator
        // this.filterOperator = new  FilterOperator();

        //using debounce time for delay x time before show next NEW value
        // this.debounceTime = new DebounceTime();

        //modifying a stream observable using (reduce, scan) that takes the last and current value from stream
        // this.scanVsReduce = new ScanVsReduce();

        //alternative for extract properties from a stream
        // this.pluck = new Pluck();

        //Combine result of 2 different streams
        // this.mergeMap = new MergeMap();

        // trigger the inner obs based on trigger of outer observable
        // this.switchMap = new SwitchMap();

        this.behaviourSubject = new CreateBehaviorSubject();
        
    }
}

let index = new Index();

