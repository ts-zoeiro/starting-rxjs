export class Component {
    private root: HTMLDivElement;
    constructor(){
        this.render();
    }

    render(): HTMLDivElement {
        this.root = document.createElement("div");
        this.root.classList.add("component");
        document.body.appendChild(this.root);
        return this.root;
    }

    appendChild(element: HTMLElement):  HTMLElement {
        this.root.appendChild(element);
        return element;
    }
}