import { Component } from "./Component";
import {Observable} from "Rxjs";

export class ScanVsReduce {
    component: Component;
    constructor(){
        this.component = new Component();
        var input = document.createElement("input");
        input.classList.add("hello");
        this.component.appendChild(input);

        var observable = Observable.of(1, 2, 3, 4, 5);

        //make the sum or another operation, using the last and current value of an array/stream
        observable
        .reduce((total: number, currentValue: number) => {
            return total + currentValue;
        }, 0)
        .subscribe({
            next: function(value: number){
                console.log(value);
            }
        });

        //scan make the same as reduce, but it returns every step of operation 
        observable
            .scan((total: number, currentValue: number) => {
                return total + currentValue;
            }, 0)
            .subscribe({
                next: function(value: number) {
                    console.log(value);
                }
            });

    }
}