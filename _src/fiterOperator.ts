import { Component } from "./Component";
import {Observable} from "Rxjs";

export class FilterOperator {
    component: Component;
    constructor() {
        this.component = new Component();

        var observable = Observable.interval(1000);

        var subscription = observable
        .filter(function (value: number) {
            return value % 2 == 0;
        })
        .subscribe({
            next: function (value: number) {
                console.log(value)
            },
            error: function (error: any) {
                console.log("ERROR: ", error)
            }
        });


        setTimeout(function(){
            console.log("it's over");
            subscription.unsubscribe();
        }, 10000)

    }
}