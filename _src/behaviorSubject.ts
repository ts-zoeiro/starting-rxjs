import {Observable, BehaviorSubject} from "Rxjs";
import { Component } from "./Component";

export class CreateBehaviorSubject {
    component: Component;

    constructor(){
        this.component = new Component();
        let button = document.createElement("button");
        button.innerHTML = "Click me";
        button.classList.add("hello");

        let div = document.createElement("div");
        div.classList.add("component");

        this.component.appendChild(button);
        this.component.appendChild(div);



        //using behavioursubject to initialize a subject with a default value
        let clickEmitted = new BehaviorSubject("Not clicked yet");
        button.addEventListener("click", () => clickEmitted.next("Clicked!"))
        clickEmitted.subscribe( (value: any) => div.textContent = value + Math.floor (Math.random() * 10) );


        

    }
} 