import { Component } from "./Component";
import {Observable} from "Rxjs";

export class Pluck {
    component: Component;
    constructor() {
        this.component = new Component();
        var input = document.createElement("input");
        input.classList.add("hello");
        this.component.appendChild(input);

        var observable = Observable.fromEvent(input, "input");

        observable
            .pluck("target", "value")
            .debounceTime(500)
            .distinctUntilChanged()
            .subscribe({
                next: (value: any) => {
                    console.log(value);
                }
            });
    }
}