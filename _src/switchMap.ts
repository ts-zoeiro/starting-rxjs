import { Component } from "./Component";
import {Observable} from "Rxjs";

export class SwitchMap {
    component: Component;
    constructor() {
        this.component = new Component;

        var button = document.createElement("button");
        button.innerHTML = "Click me";
        button.classList.add("hello");
        this.component.appendChild(button);

        var observable1 = Observable.fromEvent(button, "click");
        var observable2 = Observable.interval(1000);

        // trigger the inner obs based on trigger of outer observable

        observable1
            .switchMap((event: any) => {
                return observable2
            })
            .subscribe((value: any) => {
                console.log(value);
            });

    }
}