const merge = require("webpack-merge");
const common = require("./webpack.config");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin")

module.exports = merge(common, {
    plugins: [
        new UglifyJSPlugin({
            sourceMap: true
        })
    ], 
    mode: "production"
})